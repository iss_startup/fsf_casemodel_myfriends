// Loads express module and assigns it to a var called express
var express = require("express");

// Loads file system module and assigns it to a var called fs
var fs = require('fs');

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads util module and assigns it to a var called util
var util = require('util');

// Loads bodyParser module and assigns it to a var called bodyParser
var bodyParser = require('body-parser');

// Creates an instance of express called app
var app = express();

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.PORT || 3000;

// Define path constants
// __dirname is the absolute path of the application directory.
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const BOWER_COMPONENTS = path.join(__dirname + '/../bower_components');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/messages');

// Serves files from public directory.
// If you have not defined a handler for "/" before this line, server will look for index.html in (__dirname + "/../client")
app.use(express.static(CLIENT_FOLDER));
// Create a /bower_components handler to serve static files from (__dirname + "/../bower_components")
app.use("/bower_components", express.static(BOWER_COMPONENTS));

// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

// Create a new variable jsonDate and initialises it to an empty object
var jsonData = {};
// Reads the data.json and turns the content into valid json format. Reassigns the result back to jsonData.
fs.readFile(__dirname + '/data.json', 'utf8', function (err, data) {
    if (err) throw err;
    jsonData = JSON.parse(data);
});

// GET request handler for the path "/getData" 
app.get('/getData', function (req, res, next) {
    // Returns the jsonData object to the client
    res.send(jsonData);
});


// POST request handler for the path "/saveData" 
app.post('/saveData',function (req,res,next) {
    console.log("REQ BODY" + JSON.stringify(req.body));
    console.log('-------Data Saved Successfully-------');
    // Saves data to data.json and sends the updated file back to client side
    req.body.saved = 1;
    jsonData.push(req.body);
    res.send(jsonData);

});

// DELETE request handler for the path of '/deleteData'
app.delete('/deleteData', function (req, res, next) {
    console.log('-------Data Deleted Successfully-------');
    // Delete friend based on id
    var deleteId = req.query.id;
    // Loop through json.data
    for (var i = 0; i < jsonData.length; i++) {
        if (jsonData[i].id == deleteId) {
            // Use splice to remove from json.Data
            jsonData.splice(i, 1);
            break;
        }
    }
    // Send the update file back to client
    res.send(jsonData);
});

// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});