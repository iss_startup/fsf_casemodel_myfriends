(function () {
    // Service's constructor function to attach the service to the Angular module
    angular.module('friendsApp')
        .service('friendsService', friendsService);

    // For minification purposes, the controller needs to be annotated with the $inject property. The $inject property is an array of service names to inject.
    // This will inject $http and $q as dependencies
    friendsService.$inject = ['$http', '$q'];

    // Service function
    function friendsService($http, $q) {
        // Expose the callable members of the service in a new var svc
        var svc = {
            deleteData: deleteData,
            getData: getData,
            saveData: saveData
            
        };
        return svc;

        // Set the function to retrieve data from data.json
        function getData() {
            // Assign a new instance of deferred to a var deferred
            var deferred = $q.defer();
            // Make a GET request with the path /getData using $http.get
            $http.get('/getData')
                // this callback will be called asynchronously
                .then(function (response) {
                    // resolve the derived promise with the response
                    deferred.resolve(response);
                })
                // called asynchronously if an error occurs
                .catch(function (error) {
                    // rejects the derived promise with the error
                    deferred.reject(error);
                })
                // returns a new promise instance to the controller
            return deferred.promise;
        };


        // Set the function to save data to data.json
        function saveData(data) {
            var deferred = $q.defer();
            console.log("service data>>"+JSON.stringify(data));
            // Make a POST request with the path /saveData using $http.post
                $http.post('/saveData', data)
                    // this callback will be called asynchronously
                    .then(function (response) {
                        console.log(".then data>>"+JSON.stringify(data));
                        // resolve the derived promise with the response
                        deferred.resolve(response);
                    })
                    // called asynchronously if an error occurs
                    .catch(function (error) {
                        // rejects the derived promise with the error
                        deferred.reject(error);
                    })
                    // returns a new promise instance to the controller
                return deferred.promise;
            };
       
        // Set the function to delete data from data.json
        function deleteData(id) {
            var deferred = $q.defer();
            // Make a DELETE request with the path /saveData using $http.delete
            $http.delete('/deleteData?id=' + id)
                // this callback will be called asynchronously
                .then(function (response) {
                    // resolve the derived promise with the response
                    deferred.resolve(response);
                })
                // called asynchronously if an error occurs
                .catch(function (error) {
                    // rejects the derived promise with the error
                    deferred.reject(error);
                })
                // returns a new promise instance to the controller
            return deferred.promise;
        };
    }

}());