(function () {
    // Controller's constructor function to attach the controller to the Angular module
    angular.module("friendsApp")
        .controller("FriendsCtrl", FriendsCtrl)

    // For minification purposes, the controller needs to be annotated with the $inject property. The $inject property is an array of service names to inject.
    // This will inject the service, friendsService, as a dependency
    FriendsCtrl.$inject = ["ModalService", "friendsService"]

    // Controller function
    function FriendsCtrl(ModalService, friendsService) {
        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the friendsCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of friendsCtrl, e.g., index.html
        var vm = this;

        // Keep the bindable members of the controllers at the top
        vm.delete = deleteFromDB;
        vm.friendObject = defaultFriendObject();
        vm.friendsArray = [];
        vm.format = 'hh:mm:ss a';
        vm.orderProp = 'friendName';
        vm.query = "";
        vm.resetForm = resetForm;
        vm.saveToDB = saveToDB;
        vm.showUserDetails = showUserDetails;
        vm.friendId = 0;
        vm.status = {
            message: '',
            error: false
        };
        vm.submit = submit;

        // --------------------------------------
        // The object factory for FriendObject
        // defaultFriendObject will create a new empty FriendObject each time the function is called
        function defaultFriendObject() {
            var obj = {};
            obj.id = null;
            obj.friendName = "";
            obj.contactNo = "";
            obj.email = "";
            obj.gender = "male";
            obj.saved = 0;
            return obj;
        };

        // Function to delete friend from friendsArray
        function deleteFromDB(friendId) {
            friendsService.deleteData(friendId)
                .then(function (response) {
                    vm.friendsArray = [];
                    vm.friendsArray = response.data;
                    vm.status.message = "Item Deleted from Database ."
                })
                .catch(function (error) {
                    vm.status.error = true;
                    vm.status.message = "Error in Delete from Database ."
                })
        };

        // Calls the getData method from friendsService service
        friendsService.getData()
            .then(function (data) {
                // Assigns the data retrieved from data.json to vm.friendsArray
                vm.friendsArray = data.data;
                // Assigns a unique id to vm.friendId so there will be no repeated id
                vm.friendId = vm.friendsArray[vm.friendsArray.length - 1].id + 1;
            });

        // Function to clear the form when reset button is clicked
        function resetForm() {
            vm.friendObject = defaultFriendObject();
        };

        // Function to save friends to data.json
        function saveToDB(friend) {
            // if function to check if friend is already saved or not
            if (!friend.saved) {
                // if not save, call the saveData function rom friendsService
                friendsService.saveData(friend)
                    .then(function (response) {
                        // Clears out friendsArray and reassign the response from server back to friendsArray
                        vm.friendsArray = [];
                        vm.friendsArray = response.data;
                        vm.status.message = " Item Saved inside the Database ."
                    })
                    .catch(function (error) {
                        vm.status.error = true;
                        vm.status.message = "Error in Save inside the Database ."
                    })
            } else {
                // print out "Friend Details are already Saved" if there saved is truthy(true, 1)
                console.log("Friend Details are already Saved");
            }

        };

        // Function to configure pop up modal
        function showUserDetails(friendDetails) {
            // Set the appropriate html file as template and use showUserController as controller
            ModalService.showModal({
                templateUrl: "../app/modals/show.friend-details.modal.html",
                controller: "ShowUserController as ctrl",
                inputs: {
                    title: friendDetails
                }
            }).then(function (modal) {
                // Configuration for closing the pop up modal
                modal.element.modal();
                modal.close.then(function (result) {});
            });

        }

        // Function to process the data for submission to server
        function submit() {
            // Generate a new id for FriendObject
            if (vm.friendObject.id === null) {
                vm.friendObject.id = vm.friendId;
                vm.friendId++;
            }
            // Selects the profile picture according to gender
            if (vm.friendObject.gender === 'female') {
                vm.friendObject.imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/0/07/Avatar_girl_face.png'
            } else {
                vm.friendObject.imageUrl = 'http://i.imgur.com/1o1zEDM.png';
            }
            // Adds to the array containing all friendObjects
            vm.friendsArray.push(vm.friendObject);

            // Resets the form
            vm.friendObject = defaultFriendObject();
            // Success status message
            vm.status.message = " Item added into the list , But not saved in Database";
        };
    };
}());